//  定义登录模块
require.config({
　　　　paths: {
　　　　　　"jquery": "jquery",
　　　　　　"ajax": "ajax"
　　　　}
});

require(['jquery','ajax','verifyCode'],function($,Ajaxpost,verifyCode){
	// 登录
	$('.login').click(function(){
		var ajaxUrl = 'http://211.103.199.48:8080/app/user/v1/userLogin';
		var ajaxParam ={
			'accountName' : $('#phone').val(),
			'password' : $('#userpassword').val()
		};
		var ajaxParam = JSON.stringify(ajaxParam);
		Ajaxpost.Ajaxquery('post',ajaxUrl,ajaxParam,function(jsonData){
			if(jsonData.code == '000000'){
				var user = jsonData.data.token;
				localStorage.token = user;
				window.location.href = 'http://localhost/ph51wechat/html/account/index.html';
			}else {
				alert(jsonData.description);
			}
		});
	});	
	// 获取手机验证码
	$('#sendCode').click(function() {
		var timer = null ;
		var sendCodeUrl = 'http://211.103.199.48:8080/app/platinfo/v1/resetVerifyCodeRegister';
		var sendCodeParam ={
			'dome' : $('#regPhone').val(),
		};
		var sendCodeParam = JSON.stringify(sendCodeParam);
		Ajaxpost.Ajaxquery('post',sendCodeUrl,sendCodeParam,function(jsonData){
			if(jsonData.code == '000000'){
				var result = verifyCode.isPhoneNum();
				if(result){
					verifyCode.settime($('#sendCode'));
				}
			}else {
				alert(jsonData.description);
			}
		});
	});
	// 注册
	$('#reg').click(function() {
		var regUrl = 'http://211.103.199.48:8080/app/user/v1/userRegister';
		var regParam ={
			'onePwd' : $('#onePwd').val(),
			'inventCode' : $('#inventCode').val(),
			'phone' : $('#regPhone').val(),
			'verifyCode' : $('#verifyCode').val(),
		};
		var regParam = JSON.stringify(regParam);
		Ajaxpost.Ajaxquery('post',regUrl,regParam,function(jsonData){
			if(jsonData.code == '000000'){
				window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
			}else {
				alert(jsonData.description);
			}
		});
	});


	// 忘记密码
	$('.findPwd').click(function() {
		window.location.href = 'http://localhost/ph51wechat/html/password/forget.html'
	});
});