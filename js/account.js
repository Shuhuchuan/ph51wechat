require(['jquery','ajax'],function($,Ajaxpost){
	// 加载我的账户
	var accountUrl = 'http://211.103.199.48:8080/app/user/v1/userAccountInfo';
	var ajaxParam ={
	};
	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){
		if(jsonData.code == "000000"){
			//判断是否为老用户
			console.log(jsonData)
			//alert(jsonData.data.currUser.isBindingBank)
			//alert(jsonData.data.currUser.isBindingBank)
			sessionStorage.isNew=jsonData.data.currUser.isNew;//判断新老用户
			sessionStorage.realName=jsonData.data.currUser.realName;//真实姓名
			sessionStorage.idcard=jsonData.data.currUser.idcard;//身份证号
			sessionStorage.mobile=jsonData.data.currUser.mobile;//手机号
			//是否开通免密服务
			//alert(jsonData.data.accountInfo.payStatus)
			sessionStorage.payStatus=jsonData.data.currUser.payStatus;
			//alert(jsonData.data.accountInfo.isNew)
			// 手机号码
			$('#myMobile').html(jsonData.data.currUser.mobile);
			// 用户名
			$('#userId').html(jsonData.data.currUser.userId);
			// 累计收益
			$('.myProfit').html(jsonData.data.accountInfo.totalProfit);
			// 可用金额
			$('.myBalance').html(jsonData.data.accountInfo.available);
			sessionStorage.available=jsonData.data.accountInfo.available;
			// 资产总额
			$('.myTotal').html(jsonData.data.accountInfo.total);
			// 资金迁移金额
			$('.canMigrateAmount').html(jsonData.data.accountInfo.canMigrateAmount);
			sessionStorage.canMigrateAmount=jsonData.data.accountInfo.canMigrateAmount;

			// 判断可迁移金额是否大于0 , 大于0跳转到迁移页面
			if(jsonData.data.accountInfo.canMigrateAmount > 0){
				$('#icon_prompt').html('(未迁移)');
				$('.AmountMigrate').addClass('transferBtn');
				$('.transferBtn').bind('click',function(){
					window.location.href = 'http://localhost/ph51wechat/html/account/capitaltransfero.html';
				});
			}else if(jsonData.data.accountInfo.canMigrateAmount == 0){
				$('#icon_prompt').hide();
			}



			// 站内信
			if(jsonData.data.currUser.letterCount > 0){
				$('.letterCount').html('<i class="acc_yd"></i>');
			}
			// 银行卡开通
			
				if(jsonData.data.currUser.isBindingBank){
					$('.BindingBank').html('已认证');
					$('.BindingBank').click(function(){
						if(sessionStorage.payStatus=='true'){
							window.location.href = 'http://localhost/ph51wechat/html/bank_card.html';
						}else{
							window.location.href = 'http://localhost/ph51wechat/html/bank_card4.html';	
							}
					})
				}else {
					$('.BindingBank').html('未认证');
					$('.BindingBank').click(function(){
						window.location.href = 'http://localhost/ph51wechat/html/bank_card2.html';	
				})
			}	
			// 资金托管
			if(jsonData.data.currUser.idcardVerified!="no"){
				$('.isTrustee').html('已开通');
				$('.isTrustee').click(function(){
                   // window.location.href="http://localhost/ph51wechat/html/account/realname.html"
				});

			}else {
				$('.isTrustee').html('未开通');
				$('#isTrusteeHref').click(function(){
					window.location.href="http://localhost/ph51wechat/html/account/realname.html"
				});
			}
		}else if(jsonData.code == "200001") {
			window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
		}else {
			alert(jsonData.description);
		}

	});

	// 充值
	$('.btn_cz').click(function(){
		 window.location.href = 'http://localhost/ph51wechat/html/account/recharge.html';
	});
	// 提现
	$('.btn_tx').click(function(){
		window.location.href='http://localhost/ph51wechat/html/account/cash.html'
	});
	// 账户信息
	$('.accountInfo').click(function(){
		window.location.href = 'http://localhost/ph51wechat/htmlph51wechat/html/account/accountInfo.html';
	});
	// 债券转让
	$('.financialCreditorList').click(function() {
		window.location.href = 'http://localhost/ph51wechat/html/account/transfer.html';
	});
	// 消息/站内信
	$('.letterList').click(function() {
		window.location.href = 'http://localhost/ph51wechat/html/account/mail.html';
	});

	// 邀请
	$('.getMyInviteCode').click(function() {
		window.location.href = 'http://localhost/ph51wechat/html/request/index.html'
	});
	// 资金记录
	$('.RecordList').click(function() {
		window.location.href = 'http://localhost/ph51wechat/html/account/record.html'
	});
	// 我的理财
	$('.InvestList').click(function() {
		window.location.href = 'http://localhost/ph51wechat/html/account/mydefault.html'
	});
});