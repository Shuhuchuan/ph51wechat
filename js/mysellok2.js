require(['jquery','ajax','common'],function($,Ajaxpost,common){

	var proid = (common.GetQueryString("proId"));

	var transferUrl = 'http://211.103.199.48:8080/app/user/v1/myCreditorTransferDetail';
	var ajaxParam ={
		creditorId : proid
	};
	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',transferUrl,ajaxParam,function(jsonData){
		console.log(jsonData);
		if(jsonData.code == '000000'){
			//  债权名称
			$('#loanTitle').html(jsonData.data.loanTitle);
			$('#cred_title').html(jsonData.data.loanTitle);

			// 受让人
			$('#sellPerson').html(jsonData.data.sellPerson);
			
			//  债权编号
			$('#creNum').html(jsonData.data.creNum);
			//  转让金额
			$('#value').html(jsonData.data.value+'元');
			//  预期年化
			$('#yearRate').html(jsonData.data.yearRate+'%');

			//  出让时间
			$('#transferTimes').html(jsonData.data.transferTimes+'天');
			//  手续费
			$('#fee').html(jsonData.data.fee+'元');
			//  还款方式
			$('#repaymentType').html(jsonData.data.repaymentType);
			//  计息规划
			$('#rules').html(jsonData.data.rules);

			$('#credtedails').attr('href','http://localhost/ph51wechat/html/product/credrightstedails.html?proId='+jsonData.data.creditorId)
		
		}
		
	});
});