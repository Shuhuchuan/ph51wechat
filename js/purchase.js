
require(['jquery','ajax','common'],function($,Ajaxpost,common){

	var proid = (common.GetQueryString("proId"));

	// 请求个人中心调出帐户可用余额
	var accountUrl = 'http://211.103.199.48:8080/app/user/v1/userAccountInfo';
	var ajaxParam ={
	};

	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){

		if(jsonData.code == '000000'){
			
			$('#available').html(jsonData.data.accountInfo.available);

		}else if(jsonData.code == '200001') {
			window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
		}else{
			alert(jsonData.description);
		}
				
	});	

	//  获取标的详情信息
	var tedailsUrl = 'http://211.103.199.48:8080/app/invest/v1/loanInvestDetail';
	var tedailsParam ={
		investId : proid
	};

	var tedailsParam = JSON.stringify(tedailsParam);

	Ajaxpost.Ajaxquery('post',tedailsUrl,tedailsParam,function(jsonData){

		if(jsonData.code == '000000'){
			
			//  标的标题
			$('#bd_title').html(jsonData.data.loanObject.title);

			//  年化率			
			$('#yearRate').html('预期年化'+jsonData.data.loanObject.yearRateP+'%');	
			$('#yearRateP').html(jsonData.data.loanObject.yearRateP+'%');	
			
			//  投资期限
			$('#bidDays').html('投资期限'+jsonData.data.loanObject.repaymentMonths+'个月');

			//  还款方式
			$('#repaymentTypeDescr').html(jsonData.data.loanObject.repaymentTypeDescr);

			//  最小投标金额
			$('#amount_money').attr('placeholder','最低投资金额'+jsonData.data.minBidAmount+'元，或'+jsonData.data.minBidAmount+'的整数倍');

			$('#priceP').html('0');
			$('#profit_money').html('0');

			$('#amount_money').keyup(function(){
				var nume = $(this).val();
				if($(this).val()){
					$('#priceP').html($(this).val());
					//  计算预期收益
					$('#profit_money').html(Math.floor((nume*jsonData.data.loanObject.yearRateP/100*jsonData.data.loanObject.repaymentMonths/12)*100)/100);
				}else{
					$('#priceP').html('0');
					//  计算预期收益
					$('#profit_money').html(Math.floor((nume*jsonData.data.loanObject.yearRateP/100*jsonData.data.loanObject.repaymentMonths/12)*100)/100);
				}
			});

			if(jsonData.data.isOpenDealpwd){
				$('#mm_agree').html('');
			};

		}else {
			alert(jsonData.description);
		}
			
	});

	$('#pro_nopasswd').bind('click',function(){
		var purchaseUrl = 'http://211.103.199.48:8080/app/sign/v1/signNopasswdInvest';
		var purchaseParam ={
		};

		var purchaseParam = JSON.stringify(purchaseParam);

		Ajaxpost.Ajaxquery('post',purchaseUrl,purchaseParam,function(jsonData){

			if(jsonData.code == '000000'){
				window.location.href = jsonData.data.url;
			}				
		});	
	});

	//  购买页面接口
	$('.purchase_btn').bind('click',function(){
		
		if(!$('#agree').is(':checked')){
			alert('请先同意理财产品/散标协议');
			return false;
		}

		var amountMoney = $('#amount_money').val();

		var purchaseUrl = 'http://211.103.199.48:8080/app/invest/v1/loanForBuy';
		var purchaseParam ={
			investId : proid,
			amount : amountMoney
		};

		var purchaseParam = JSON.stringify(purchaseParam);

		Ajaxpost.Ajaxquery('post',purchaseUrl,purchaseParam,function(jsonData){
			
			if(jsonData.code == '000000'){
				console.log(jsonData);
				window.location.href = jsonData.data.url;
				
			}else if(jsonData.code == '200012'){
				alert(jsonData.description);
			}else{
				alert(jsonData.description);
			}
				
		});		
	});	
});