//   基础模块


define(function(){
	var Ajaxquery = function(the_type, the_url, the_param, succ_callback) {
		if (!the_url) {
			return false;
		}
		if (!the_type) {
			the_type = "post";
		}
		the_type = the_type.toLowerCase();
		$.ajax({
			"headers" : {
				"opSource" : "wx",
				"Authorization" : localStorage.getItem("token")
			},
			"type" : the_type,
			"cache" : false,
			"url" : the_url,
			"data" : the_param,
			"dataType" : "json",
			"contentType": "application/json",
			"xhrFields" : {
				"withCredentials" : true
			},
			"crossDomain" : true,
			"success" : succ_callback,
			"error" : function() {
				alert("服务器连接错误");
			}
		});
	};

	return {
		Ajaxquery : Ajaxquery
	}

});