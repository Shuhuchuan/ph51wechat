
require(['jquery','ajax','common'],function($,Ajaxpost,common){

	var proid = (common.GetQueryString("proId"));

	// 请求个人中心调出帐户可用余额
	var accountUrl = 'http://211.103.199.48:8080/app/user/v1/userAccountInfo';
	var ajaxParam ={
	};

	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){
		console.log(jsonData)
		if(jsonData.code == '000000'){
			
			$('#available').html(jsonData.data.accountInfo.available);

		}else if(jsonData.code == '200001') {
			window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
		}else{
			alert(jsonData.description);
		}
				
	});	

	//  获取债权转让可转让的详情信息
	var tedailsUrl = 'http://211.103.199.48:8080/app/user/v1/myCreditorTransferDetail';
	var tedailsParam ={
		creditorId : proid
	};

	var tedailsParam = JSON.stringify(tedailsParam);

	Ajaxpost.Ajaxquery('post',tedailsUrl,tedailsParam,function(jsonData){
		console.log(jsonData);
		if(jsonData.code == '000000'){
			//  债权转让标题
			$('#bd_title').html(jsonData.data.loanTitle);
			$('#value').html('债权价值'+jsonData.data.value+'元')
			//  年化率
			$('#yearRate').html('预期年化'+jsonData.data.yearRate+'%');
			//   还款方式
			$('#repaymentType').html(jsonData.data.repaymentType);
			$('#number').html('剩余期数'+jsonData.data.number);
			
			$('#amount_money').attr('placeholder',jsonData.data.value);

			//  购买页面接口
			$('#purchase_btn').bind('click',function(){
				
				if(!$('#agree').is(':checked')){
					alert('请先同意债权转让协议');
					return false;
				}

				if(parseInt($('#amount_money').val()) > jsonData.data.value){
					alert('转让价格不得高于债权价值');
					return false;
				}

				var amountMoney = $('#amount_money').val();

				var purchaseUrl = 'http://211.103.199.48:8080/app/user/v1/financialTransferCreditor';
				var purchaseParam ={
					creditorId : proid,
					amount : amountMoney
				};

				var purchaseParam = JSON.stringify(purchaseParam);

				Ajaxpost.Ajaxquery('post',purchaseUrl,purchaseParam,function(jsonData){
					
					if(jsonData.code == '000000'){
						alert('发起债权转让成功!')
						window.location.href = 'http://localhost/ph51wechat/html/account/transfer.html';
						
					}else if(jsonData.code == '200012'){
						alert(jsonData.description);
					}else{
						alert(jsonData.description);
					}
						
				});		
			});

		}else {
			alert(jsonData.description);
		}
			
	});	
});