require(['jquery','ajax','common'],function($,Ajaxpost,common){
	

	var proid = (common.GetQueryString("proId"));

	common.offOn($('.pro_d p'),$('.pro_d_r'),$('wx_current'));

	//  投资详情页接口

	var accountUrl = 'http://211.103.199.48:8080/app/invest/v1/loanInvestDetail';
	var ajaxParam ={
		investId : proid
	};

	var ajaxParam = JSON.stringify(ajaxParam);

	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){

		if(jsonData.code == '000000'){
			console.log(jsonData);
			//  年化率			
			$('#yearRate').html(jsonData.data.loanObject.yearRateP+'<em>%</em><b>+'+jsonData.data.loanObject.rewardRate+'%</b>');			

			// 投资进度
			$('#proess').html('<span><em style="width:'+jsonData.data.loanObject.proess+'%"></em></span>'+jsonData.data.loanObject.proess+'%');
			
			//  投资期限
			$('#bidDays').html('投资期限'+jsonData.data.loanObject.repaymentMonths+'个月');

			//  还款方式
			$('#repaymentTypeDescr').html(jsonData.data.loanObject.repaymentTypeDescr);

			//  最小投标金额
			$('#minBidAmount').html(jsonData.data.minBidAmount+'元起投')

			//  贷款总额
			$('#amount').html(jsonData.data.loanObject.amount+'.00');

			//  剩余贷款总额
			$('#remainAmount').html(jsonData.data.loanObject.remainAmount+'.00');
			
			//  借款人信息
			$('#leonName').html(jsonData.data.userInfo.leonName);
			$('#sex').html(jsonData.data.userInfo.sex);
			$('#age').html(jsonData.data.userInfo.age);
			$('#address').html(jsonData.data.userInfo.address);

			if(jsonData.data.loanObject.type == 'mortgage'){
				$('#leonName_type').html($('#leonName_type').html()+'<li class="clearfix">借款详情</br><span class="">'+jsonData.data.userInfo.loanDetail+'</span></li><li class="clearfix">还款来源</br><span class="">'+jsonData.data.userInfo.payment+'</span></li>');
				$('#proicon_box').html('<li><img src="../../images/wx_pro1.png" alt=""><br>身份证</li><li><img src="../../images/wx_pro12.png" alt=""><br>公证书</li><li><img src="../../images/wx_pro14.png" alt=""><br>抵押证明</li><li><img src="../../images/wx_pro15.png" alt=""><br>打款证明</li><li><img src="../../images/wx_pro10.png" alt=""><br>合同</li><li><img src="../../images/wx_pro11.png" alt=""><br>其它</li>');
			};
			
			//  投资列表
			$('#deposits_inow').html('('+jsonData.data.userInfo.deposits.length+')');

			var depositshtml = null;
			if(jsonData.data.userInfo.deposits.length > 0){
				for (var i = 0; i < jsonData.data.userInfo.deposits.length; i++) {
					depositshtml += '<tr><td>'+jsonData.data.userInfo.deposits[i].name+'</td><td>'+jsonData.data.userInfo.deposits[i].amount+'</td><td>'+jsonData.data.userInfo.deposits[i].bidTimeString+'</td></tr>';
				};
				$('#deposits').html(depositshtml);
			}else{
				$('#deposits').html('<tr><td colspan="3">暂时还没有数据</td></tr>');
			};

			//   购买按钮
			if(jsonData.data.loanObject.remainAmount > 0){

				if(sessionStorage.prokey == 0){
					$('.wechat_btn').addClass('color_btn');
					$('.wechat_btn').html('预热中');
				}else{
					$('.wechat_btn').addClass('purchase_b');
					$('.wechat_btn').html('抢购');
					
					$('.purchase_b').bind('click',function(){
						if(localStorage.getItem("token")){
							window.location.href = 'http://localhost/ph51wechat/html/product/purchase.html?proId='+proid;
						}else{
							window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
						}
					});
				}
			}else{
				$('.wechat_btn').html('售罄');
			};


		}else {
			alert(jsonData.description);
		}
			
	});


});