require(['jquery','ajax','common'],function($,Ajaxpost,common){

	var proId = common.GetQueryString("proId");
	
	var transferUrl = 'http://211.103.199.48:8080/app/user/v1/myCreditorTransferDetail';
	var ajaxParam ={
		creditorId : proId
	};
	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',transferUrl,ajaxParam,function(jsonData){
		console.log(jsonData);
		if(jsonData.code == '000000'){
			// 借款标题
			$('#loanTitle_t').html(jsonData.data.loanTitle);
			$('#loanTitle').html(jsonData.data.loanTitle);

			$('#transformOutPerson').html(jsonData.data.transformOutPerson);
			
			//  标号
			$('#creNum').html(jsonData.data.creNum);
			//  投资金额
			$('#amount').html(jsonData.data.amount+'元');

			//  预期年化
			$('#yearRate').html(jsonData.data.yearRate+'%');
			//  预期收益
			$('#income').html(jsonData.data.income+'元');
			//  投资时间
			$('#dealTimeString').html(jsonData.data.dealTimeString);

			//  还款方式
			$('#repaymentType').html(jsonData.data.repaymentType);
			//  投资期限
			$('#number').html(jsonData.data.number+'期');
			//  计息规划
			$('#rules').html(jsonData.data.rules);

			$('#prodateils_btn').attr('href','http://localhost/ph51wechat/html/product/credrightstedails.html?proId='+jsonData.data.creditorId);
		}
		
	});
});