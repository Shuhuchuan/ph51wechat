//   公共方法模块


define(function(){

	var toDownUp = function (obj,oDiv,aClass) {
		if(obj.siblings(oDiv).is(':hidden')){
			obj.parent().addClass(aClass);
			obj.siblings(oDiv).slideDown();
		}else{
			obj.parent().removeClass(aClass);
			obj.siblings(oDiv).slideUp();
		}
	};

	var offOn = function  (obj,oDiv,aClass) {
		$(obj).on('click',function () {
			_this = $(this);
			toDownUp(_this,oDiv,aClass);
		});	
	};

	// 截取链接
	var GetQueryString = function (name)
	{
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	}
	
	return {
		toDownUp : toDownUp,
		offOn : offOn,
		GetQueryString : GetQueryString

	}

});