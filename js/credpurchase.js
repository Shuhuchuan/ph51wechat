
require(['jquery','ajax','common'],function($,Ajaxpost,common){

	var proid = (common.GetQueryString("proId"));

	// 请求个人中心调出帐户可用余额
	var accountUrl = 'http://211.103.199.48:8080/app/user/v1/userAccountInfo';
	var ajaxParam ={
	};

	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){

		if(jsonData.code == '000000'){
			console.log(jsonData);
			console.log(jsonData.data.accountInfo.available);

		}else if(jsonData.code == '200001') {
			window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
		}else{
			alert(jsonData.description);
		}
				
	});	

	//  获取债权转让详情信息
	var tedailsUrl = 'http://211.103.199.48:8080/app/invest/v1/creditorTransferDetail';
	var tedailsParam ={
		investId : proid
	};

	var tedailsParam = JSON.stringify(tedailsParam);

	Ajaxpost.Ajaxquery('post',tedailsUrl,tedailsParam,function(jsonData){

		if(jsonData.code == '000000'){
			console.log(jsonData);
			//  债权转让的标题
			$('#bd_title').html(jsonData.data.loanObject.title);

			//  债权转让金额
			$('#price').html('转让价格'+jsonData.data.debtTransfer.price+'元');
			$('#priceP').html(jsonData.data.debtTransfer.price);

			//  债权转让的年化率			
			$('#yearRate').html('预期年化'+jsonData.data.loanObject.yearRateP+'%');	
			$('#yearRateP').html(jsonData.data.loanObject.yearRateP+'%');

			//  计算预期收益
			$('#profit_money').html(Math.floor((jsonData.data.debtTransfer.price*jsonData.data.loanObject.yearRateP/100*jsonData.data.debtTransfer.remainPeriodNo/12)*100)/100);
			
			//  债权转让的剩余期限
			$('#bidDays').html('剩余期数 '+jsonData.data.debtTransfer.remainPeriodNo+'/'+jsonData.data.loanObject.repaymentMonths+'期');

			//  债权转让的还款方式
			$('#repaymentTypeDescr').html(jsonData.data.loanObject.repaymentTypeDescr);

			if(jsonData.data.isOpenDealpwd){
				$('#mm_agree').html('');
			};

		}			
	});


	$('#pro_nopasswd').bind('click',function(){
		var purchaseUrl = 'http://211.103.199.48:8080/app/sign/v1/signNopasswdInvest';
		var purchaseParam ={
		};

		var purchaseParam = JSON.stringify(purchaseParam);

		Ajaxpost.Ajaxquery('post',purchaseUrl,purchaseParam,function(jsonData){

			if(jsonData.code == '000000'){
				window.location.href = jsonData.data.url;
			}				
		});	
	});

	//  购买页面接口
	$('.purchase_btn').bind('click',function(){

		if(!$('#agree').is(':checked')){
			alert('请先同意债权转让协议');
			return false;
		};

		var purchaseUrl = 'http://211.103.199.48:8080/app/invest/v1/creditorForBuy';
		var purchaseParam ={
			investId : proid
		};

		var purchaseParam = JSON.stringify(purchaseParam);

		Ajaxpost.Ajaxquery('post',purchaseUrl,purchaseParam,function(jsonData){
			
			if(jsonData.code == '000000'){
				console.log(jsonData);
				window.location.href = jsonData.data.url;

			}else if(jsonData.code == '200012'){
				alert(jsonData.description);
			}else{
				alert(jsonData.description);
			}
				
		});		
	});	
});