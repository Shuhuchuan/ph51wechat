require(['jquery','ajax','common'],function($,Ajaxpost,common){

	var proId = common.GetQueryString("proId");
	
	var transferUrl = 'http://211.103.199.48:8080/app/user/v1/financialInvestDetail';
	var ajaxParam ={
		creator : sessionStorage.credid,
		id : proId,
		prodId : sessionStorage.myrepay
	};
	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',transferUrl,ajaxParam,function(jsonData){
		console.log(jsonData);
		if(jsonData.code == '000000'){
			// 借款标题
			$('#investTitle_t').html(jsonData.data.investTitle);
			$('#investTitle').html(jsonData.data.investTitle);
			//  借款标号
			$('#investNum').html(jsonData.data.investNum);
			//  投资金额
			$('#price').html(jsonData.data.price+'元');
			//  待收本金
			$('#principal').html(jsonData.data.principal+'元');
			//  待收收益
			$('#interest').html(jsonData.data.interest+'元');
			//  预期年化
			$('#yearRate').html(jsonData.data.yearRate+'%');
			//  预期收益
			$('#earnings').html(jsonData.data.earnings+'元');
			//  投资时间
			$('#investmentDate').html(jsonData.data.investmentDate);
			//  还款方式
			$('#refundType').html(jsonData.data.refundTypeString);
			//  投资期限
			$('#zcout').html(jsonData.data.zcout+'个月');
			//  计息规划
			$('#rules').html(jsonData.data.rules);
			//  开始时间
			$('#startTimeDate').html(jsonData.data.startTimeDate);
			//  结束时间
			$('#completeTimeDate').html(jsonData.data.completeTimeDate);


			function isstatus(name){
				if(name != 0){
					return '<span class="zhuan yu fl">逾</span>';
				}else{
					return '';
				}
			};

			var myrepayhtml = '';
			for(var i=0; i<jsonData.data.benifitPlan.length; i++){
				console.log(jsonData.data.benifitPlan[i].status);
				myrepayhtml += '<ul class="clearfix"><li class="fl">'+(i+1)+'</li><li class="fl">'+jsonData.data.benifitPlan[i].benifitDate+'</li><li class="fr">'+isstatus(jsonData.data.benifitPlan[i].status)+jsonData.data.benifitPlan[i].amount+'元</li></ul>'
			}

			$('.myrepay_box').html(myrepayhtml);
			$('#prodateils_btn').attr('href','http://localhost/ph51wechat/html/product/protedails.html?proId='+jsonData.data.id);
		}
		
	});
});