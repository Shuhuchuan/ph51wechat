
define(function(){
	//开始倒计时
	var countdown = 60;
	var settime = function (obj) { 
	    if (countdown == 1) { 
	    	clearTimeout(timer);
	        obj.removeAttr("disabled");    
	        obj.val("获取验证码"); 
	        return;
	    } else { 
	    	countdown--;
	        obj.attr("disabled", true); 
	        obj.val("重新发送(" + countdown + ")"); 
	        timer = setTimeout(function() { settime(obj) },1000); //每1000毫秒执行一次
	        
	    } 
	    
	}

	//校验手机号是否合法
	var isPhoneNum = function (){
	    var phonenum = $('#regPhone').val();
	    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/; 
	    if(!myreg.test(phonenum)){ 
	        alert('请输入有效的手机号码！'); 
	        return false; 
	    }else{
	        return true;
	    }
	}
	return {
		settime : settime,
		isPhoneNum : isPhoneNum
	}
});
 
		