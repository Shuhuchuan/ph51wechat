function toDownUp(obj,oDiv,aClass) {
	if(obj.siblings(oDiv).is(':hidden')){
		obj.parent().addClass(aClass);
		obj.siblings(oDiv).slideDown();
	}else{
		obj.parent().removeClass(aClass);
		obj.siblings(oDiv).slideUp();
	}
}
// 展开、收缩
function offOn (obj,oDiv,aClass) {
	$(obj).on('tap',function () {
		_this = $(this);
		toDownUp(_this,oDiv,aClass);
	});	
}
// 消息
function showHide(obj,oDiv,aClass){
	$(obj).on('tap',function(){
		_this = $(this);
		if(!_this.parent().attr('class','wx_grey')){
			_this.addClass('wx_grey');	
		}
		
		toDownUp(_this,oDiv,aClass);
	})
}
$(function(){
	// productsDown.html 
	offOn('.pro_d p','.pro_d_r','wx_current');
	//邀请-我的邀请.html
	offOn('.invite_month','.invite_list','show_invi');
 

	// news.html
	showHide('.wx_news ul li p','.wx_news_tips','wx_current');
})

/*$(function(){
	$(".invite_month").click(function(){
		$(this).parent().find(".invite_list").toggleClass("show_invi")
	})
})*/