define(['jquery','ajax','verifyCode'],function($,Ajaxpost,verifyCode){
	// 获取手机验证码
	$('#sendCode').click(function() {
		//开始倒计时
		var timer = null ;
		var sendCodeUrl = 'http://211.103.199.48:8080/app/platinfo/v1/resetVerifyCodeNoLogin';
		var sendCodeParam ={
			'dome' : $('#regPhone').val()
		};
		var sendCodeParam = JSON.stringify(sendCodeParam);
		Ajaxpost.Ajaxquery('post',sendCodeUrl,sendCodeParam,function(jsonData){
			console.log(jsonData)
			if(jsonData.code == '000000'){
				var result = verifyCode.isPhoneNum();
				if(result){
					verifyCode.settime($('#sendCode'));
				}
			}else {
				console.log(jsonData.description);
			}
		});
	});
	// 找回密码
	$('#forgetFwd').click(function(){
		var forgetUrl = 'http://211.103.199.48:8080/app/platinfo/v1/resetFindPwd';
		var forgetParam ={
			'newPwd' : $('#password').val(),
			'userId' : localStorage.userId
		};

		var forgetParam = JSON.stringify(forgetParam);

		Ajaxpost.Ajaxquery('post',forgetUrl,forgetParam,function(jsonData){
			if(jsonData.code == '000000'){
				delete localStorage.token;
				delete localStorage.userId;
				window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
			}else {
				alert(jsonData.description);
			}
		});
	});
});