require(['jquery','ajax','common'],function($,Ajaxpost,common){
	
	var proid = (common.GetQueryString("proId"));
	//  债权转让详情接口

	var accountUrl = 'http://211.103.199.48:8080/app/invest/v1/creditorTransferDetail';
	var ajaxParam ={
		investId : proid
	};

	var ajaxParam = JSON.stringify(ajaxParam);

	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){

		if(jsonData.code == '000000'){
			
			
			//转让金额
			$('#cred_money').html(jsonData.data.debtTransfer.price+'.00');
			//原标期限
			$('#repaymentMonths').html(jsonData.data.loanObject.repaymentMonths+'个月');
			//年利率
			$('#yearRateP').html(jsonData.data.loanObject.yearRateP+'%');
			//剩余期数
			$('#remainPeriodNo').html(jsonData.data.debtTransfer.remainPeriodNo+'/'+jsonData.data.loanObject.repaymentMonths);
			//出让人
			$('#transferName').html(jsonData.data.debtTransfer.transferName);
			//还款方式
			$('#repaymentTypeDescr').html(jsonData.data.loanObject.repaymentTypeDescr);
			//转让开始
			$('#createTime').html(jsonData.data.debtTransfer.createTimeStr);
			//转让结束
			$('#transferTime').html(jsonData.data.debtTransfer.transferTimeStr);
			//借款人姓名
			$('#borrowName').html(jsonData.data.borrowUser.borrowName);
			//借款人性别
			$('#borrowSex').html(jsonData.data.borrowUser.borrowSex);
			//借款人年龄
			$('#borrowAge').html(jsonData.data.borrowUser.borrowAge);
			$('#proId').attr('href',('protedails.html?proId='+jsonData.data.loanObject.id));

			//   购买按钮			
			$('.cred_purchasebtn').bind('click',function(){
				if(localStorage.getItem("token")){
					window.location.href = 'http://localhost/ph51wechat/html/product/credpurchase.html?proId='+proid;
				}else{
					window.location.href = 'http://localhost/ph51wechat/html/login/index.html';
				}
			});
		}else {
			alert(jsonData.description);
		}
			
	});


});