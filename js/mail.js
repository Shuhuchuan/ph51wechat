require(['jquery','ajax'],function($,Ajaxpost){
	// 站内信
	var mailUrl = 'http://211.103.199.48:8080/app/user/v1/letterList';
	var ajaxParam ={
	};
	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',mailUrl,ajaxParam,function(jsonData){
		var result = '';
		for(var i in jsonData.data.list) {
			result += '<li data-id="'+jsonData.data.list[i].id+'" class="'+jsonData.data.list[i].flag+'"><p><span class="wx_grey">'+jsonData.data.list[i].title+'</span><span class="wx_time wx_size10">'+jsonData.data.list[i].createTimeStr+'</span></p><div class="wx_news_tips">'+jsonData.data.list[i].content+'</div></li>';
		}
		
		$('.wx_news ul').append(result);

		$('.wx_news ul li').live('click',function(){
			var flag = $(this).attr('class');
			_this = $(this);
			if(_this.attr('class') == 'unread'){
				var mailChangeUrl = 'http://211.103.199.48:8080/app/user/v1/letterForChange';
				var mailChangeParam ={
					'letterIds' : $(this).attr('data-id'),
					'updateStatus' : $(this).attr('class')
				};
				var mailChangeParam = JSON.stringify(mailChangeParam);
				Ajaxpost.Ajaxquery('post',mailChangeUrl,mailChangeParam,function(jsonData){
					_this.removeClass('unread')
					_this.addClass('read');
				});
			}

			if($(this).find('div').is(':hidden')){
				$(this).addClass('wx_current');
				if($(this).siblings().find('.wx_current')){
					$(this).siblings().removeClass('wx_current');
					$(this).siblings().find('div').slideUp('400');
				}
				$(this).find('div').slideDown('400');
			}else {
				$(this).removeClass('wx_current');
				$(this).find('div').slideUp('400');
			}
		});
	});
});