require(['jquery','ajax'],function($,Ajaxpost){
	var transferUrl = 'http://211.103.199.48:8080/app/user/v1/financialCreditorList';
	var ajaxParam ={
		'status' : '2'
	};
	var ajaxParam = JSON.stringify(ajaxParam);
	Ajaxpost.Ajaxquery('post',transferUrl,ajaxParam,function(jsonData){
		var transferDate = jsonData.data.list;
		var htmltab = null;
		function isstatus (name){
			if(name == 'holding'){
				return '持有中';
			}else if(name == 'transferring'){
				return '转让中';
			}else if(name == 'transfered'){
				htmltab = 'sellok2';
				return '已转让';
			}else if(name == 'paid'){
				return '已还清';
			}
		};

		if(transferDate != ''){
			var result = '';
			for(var i = 0; i<transferDate.length; i++){
				if(transferDate[i].status == 'transferring'){
					htmltab = 'sellok';
				}else if(transferDate[i].status == 'transfered'){
					htmltab = 'sellok2';
				};
				result += '<a href="'+htmltab+'.html?proId='+transferDate[i].oldCreId+'"><div class="zqzr_kzr"><h2>'+transferDate[i].projectName+'</h2><ul class="clearfix sell"><li>转让金额&nbsp;'+transferDate[i].value+'元</li></ul><ul class="clearfix sell"><li>出让时间&nbsp;'+transferDate[i].transferDate+'</li><li class="last"><p>'+isstatus(transferDate[i].status)+'</p></li></ul></div></a>';
			}
			$('.transfer').append(result);
		}else {
			$('.transfer').html('<p class="mt_40 text_center">暂无数据</p>')
		}
		
	});
	// 买入
	$('.Creditor1').click(function(){
		window.location.href = 'http://localhost/ph51wechat/html/account/purchase.html';
		
	});
	// 卖出
	$('.Creditor').click(function(){
		window.location.href = 'http://localhost/ph51wechat/html/account/transfer.html';
	});
});