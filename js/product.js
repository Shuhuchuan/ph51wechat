require(['jquery','ajax','circliful'],function($,Ajaxpost,circliful){
	

	//  投资列表接口

	var accountUrl = 'http://211.103.199.48:8080/app/invest/v1/loanInvestList';
	var ajaxParam ={
	};

	var ajaxParam = JSON.stringify(ajaxParam);

	Ajaxpost.Ajaxquery('post',accountUrl,ajaxParam,function(jsonData){

		if(jsonData.code == '000000'){
			var iNow_cntent = null;
			var iNowcontent = '';
			function todou (num){
				if(num < 9){
					return '0'+num;
				}else{
					return ''+num;
				}
			};
			function islisttype (istype){
				if(istype == 'credit'){
					return '<span class="di xin">信</span>';
				}else if(istype == 'mortgage'){
					return '<span class="di">抵</span>';
				}else{
					return '';
				}
			};
			function isyearRate(isyear){
				if(isyear > 0){
					 return '+'+isyear;
				}else{
					return '';
				}
			};

			function iscountdowm (deta,proess){
				var inow = parseInt(proess);
				var s= deta,d,h;
				if(s == 0){
					if(inow < 100){
						return '<div class="demo"><div class="myStat" data-dimension="70" data-text="抢购" data-info="New Clients" data-width="1" data-fontsize="16" data-percent="'+proess+'" data-fgcolor="#fd8023" data-bgcolor="#bfd6d7" data-fill="#fff"></div><div style="clear:both"></div></div>';
						
					}else{
						return '<div class="demo"><div class="myStat" data-dimension="70" data-text="售罄" data-info="New Clients" data-width="1" data-fontsize="16" data-percent="0" data-fgcolor="#fd8023" data-bgcolor="#bfd6d7" data-fill="#fff" style="color:#999"></div><div style="clear:both"></div></div>';
					}

				}else if(s > 3600){				
					 d = parseInt(s/86400);
					 s = s%86400;
					 h = parseInt(s/3600);
					return '<p>投资倒计时</p><p class="countdown"><b>'+todou(d)+'</b>天<b>'+todou(h)+'</b>时</p>';
				}else if(s <= 3600){
						m = parseInt(s/60);
					return '<p>投资倒计时</p><p class="countdown"><b>'+todou(h)+'</b>时<b>'+todou(m)+'</b>分</p>';
				}
			}

			for (var i = 0; i < jsonData.data.list.length; i++) {

				if(jsonData.data.list[i].countDown == 0){
					iNow_cntent = 1;
				}else{
					iNow_cntent = 0;
				}

				iNowcontent += '<div class="bg list_too sb_list"><a data-text="'+iNow_cntent+'" class="linkbox" href="protedails.html?proId='+jsonData.data.list[i].id+'"></a><div class="clearfix"><h2 class="wx_Bigblack padding5">'+jsonData.data.list[i].title+'</h2>'+islisttype(jsonData.data.list[i].type)+'</div><div class="pro_show cash_bank clearfix"><ul><li><p>预期年化</p><p>'+jsonData.data.list[i].yearRateP+'<span>'+isyearRate(jsonData.data.list[i].rewardRate)+'</span><em>%</em></p></li><li><p>投资期限</p><p>'+jsonData.data.list[i].repaymentMonths+'<em>个月</em></p></li><li>'+iscountdowm(jsonData.data.list[i].countDown,jsonData.data.list[i].proess)+'</li></ul></div></div>';
			};			

			$('.paddingB3').html(iNowcontent);
			
			setTimeout(function(){
				$('.myStat').circliful();
			},200);

			//  种值标识符
			$('.linkbox').bind('click',function(){
				sessionStorage.prokey = $(this).attr("data-text");
			});
			
			
		}else {
			alert(jsonData.description);
		}
			
	});


});